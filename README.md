# 文件解释

`new_nocoll.xml`:由装配体自动导出的文件

`beginner.m`:结合`new_nocoll.xml`用来生成一个基础的simulink文件

`new_nocoll.slx`:在基础simulink的基础上，封装模型并加入反馈控制

为了得到文章中的运行结果，直接运行`new_nocoll.slx`即可



参考文章：

[LQR的理解与运用 第二期——一阶倒立摆在matlab上的LQR实现_倒立单摆模型lqr-CSDN博客](https://blog.csdn.net/weixin_51772802/article/details/128901047)

[【工具篇】 Solidworks导出xml模型到matlab及simscape一系列输入输出参数设置_sw配置simscope-CSDN博客](https://blog.csdn.net/weixin_51772802/article/details/128904726)